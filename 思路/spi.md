完成自动发现 --> jdbc  --> 自动发现 jdbc的驱动的
Class.forName("具体的驱动") 尝试主动加载具体的驱动，不需要主动加载
spi --> 主动发现某一项服务  service provider interface

1、默认配置
compress  code（1） type（gzip） impl（GzipCompressor）

// 配置信息-->序列化协议
private String serializeType = "jdk";
private Serializer serializer = new JdkSerializer();

// 配置信息-->压缩使用的协议
private String compressType = "gzip";
private Compressor compressor = new GzipCompressor();

2、通过spi加载   ->
configuration.setCompressor(compressor);

3、通过xml进行配置  ->  
configuration.setCompressType(resolveCompressType(doc, xpath));
configuration.setCompressor(resolveCompressCompressor(doc, xpath));


4、java代码进行配置
RpcBootstrap.getInstance()
.application("first-roc-consumer")
.registry(new RegistryConfig("zookeeper://127.0.0.1:2181"))
.serialize("hessian")
.compress("gzip")
.reference(reference);


-- 看看具体的传输时如果获取
CompressorFactory.getCompressor(
RpcBootstrap.getInstance().getConfiguration().getCompressType()
).getCode();

SerializerFactory.getSerializer(
RpcBootstrap.getInstance().getConfiguration().getSerializeType()
).getCode()


RpcRequest rocRequest = RpcRequest.builder()
.requestId(RpcBootstrap.getInstance().getConfiguration().getIdGenerator().getId())
.compressType(CompressorFactory.getCompressor(RpcBootstrap.getInstance().getConfiguration().getCompressType()).getCode())
.requestType(RequestType.REQUEST.getId())
.serializeType(SerializerFactory.getSerializer(RpcBootstrap.getInstance().getConfiguration().getSerializeType()).getCode())
.timeStamp(new Date().getTime())
.requestPayload(requestPayload)
.build();



RpcBootstrap.getInstance().getConfiguration().getSerializeType()？
spi和xml，如果适配的问题？
spi 特殊的格式  code+type+impl --> objectWrapper --> 统一放入工厂
xml
重要的配置就是一个type