package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/30
 **/
public class LoadBalancerException extends RuntimeException {

    public LoadBalancerException(String message) {
        super(message);
    }

    public LoadBalancerException() {
    }
}
