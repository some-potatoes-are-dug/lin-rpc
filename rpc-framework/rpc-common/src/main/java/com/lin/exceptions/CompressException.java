package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/30
 **/
public class CompressException extends RuntimeException{

    public CompressException() {
    }

    public CompressException(String message) {
        super(message);
    }

    public CompressException(Throwable cause) {
        super(cause);
    }
}
