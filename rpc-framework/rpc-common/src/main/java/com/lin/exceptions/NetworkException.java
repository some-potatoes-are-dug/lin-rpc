package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/10
 **/
public class NetworkException extends RuntimeException{
    public NetworkException() {
    }

    public NetworkException(Throwable cause) {
        super(cause);
    }
    public NetworkException(String message) {
        super(message);
    }
}
