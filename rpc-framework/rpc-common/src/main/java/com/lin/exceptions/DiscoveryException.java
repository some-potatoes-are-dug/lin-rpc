package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/10
 **/
public class DiscoveryException extends RuntimeException{

    public DiscoveryException() {
    }

    public DiscoveryException(String message) {
        super(message);
    }

    public DiscoveryException(Throwable cause) {
        super(cause);
    }
}
