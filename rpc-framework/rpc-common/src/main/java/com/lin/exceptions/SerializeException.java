package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/27
 **/
public class SerializeException extends RuntimeException{
    public SerializeException() {
    }

    public SerializeException(String message) {
        super(message);
    }

    public SerializeException(Throwable cause) {
        super(cause);
    }
}
