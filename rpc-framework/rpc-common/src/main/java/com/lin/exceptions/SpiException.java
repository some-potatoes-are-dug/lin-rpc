package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/10/14
 **/
public class SpiException extends RuntimeException {

    public SpiException(String message) {
        super(message);
    }
}
