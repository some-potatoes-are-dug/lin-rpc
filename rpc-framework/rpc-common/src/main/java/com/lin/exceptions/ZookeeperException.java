package com.lin.exceptions;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/9
 **/
public class ZookeeperException extends RuntimeException{
    public ZookeeperException() {
    }

    public ZookeeperException(Throwable cause) {
        super(cause);
    }
}
