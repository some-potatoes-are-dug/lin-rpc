package com.lin.core;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.LongAdder;

/**
 * @Author: wanglin
 * @DateTime: 2023/10/21
 **/
public class ShutDownHolder {
    //标记请求
    public static AtomicBoolean BAFFLE = new AtomicBoolean(false);
    //请求计数器
//    public static CountDownLatch REQUEST_COUNTER = new CountDownLatch(0);
    public static LongAdder REQUEST_COUNTER = new LongAdder();
}
