package com.lin.config;

import com.lin.compress.Compressor;
import com.lin.compress.CompressorFactory;
import com.lin.loadbalancer.LoadBalancer;
import com.lin.serialize.Serializer;
import com.lin.serialize.SerializerFactory;
import com.lin.spi.SpiHandler;

import java.util.List;

/**
 * @Author: wanglin
 * @DateTime: 2023/10/14
 **/
public class SpiResolver {

    /**
     * 通过spi的方式加载配置项
     * @param configuration 配置上下文
     */
    public void loadFromSpi(Configuration configuration) {

        List<ObjectWrapper<LoadBalancer>> loadBalancerWrappers = SpiHandler.getList(LoadBalancer.class);
        // 将其放入工厂
        if(loadBalancerWrappers != null && loadBalancerWrappers.size() > 0){
            configuration.setLoadBalancer(loadBalancerWrappers.get(0).getImpl());
        }

        List<ObjectWrapper<Compressor>> objectWrappers = SpiHandler.getList(Compressor.class);
        if(objectWrappers != null){
            objectWrappers.forEach(CompressorFactory::addCompressor);
//            objectWrappers.forEach(objectWrapper -> {
//                CompressorFactory.addCompressor(objectWrapper);
//            });
        }

        List<ObjectWrapper<Serializer>> serializerObjectWrappers = SpiHandler.getList(Serializer.class);
        if (serializerObjectWrappers != null){
            serializerObjectWrappers.forEach(SerializerFactory::addSerializer);
        }
    }
}