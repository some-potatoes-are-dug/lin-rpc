package com.lin;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/10
 **/
public class ProtocolConfig {
    private String protocolName;

    public ProtocolConfig(String protocolName) {
        this.protocolName = protocolName;
    }
}
