package com.lin.loadbalancer;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * 负载均衡器的接口
 * @Author: wanglin
 * @DateTime: 2023/9/30
 **/
public interface LoadBalancer {
    //根据服务列表找到一个可以用的服务

    /**
     * 根据服务名获取一个可用的服务
     * @param serviceName 服务名称
     * @return 服务地址
     */
    InetSocketAddress selectServiceAddress(String serviceName,String group);


    /**
     * 感知节点发生上下线，重新负载均衡
     * @param serviceName 服务名称
     */
    void reLoadBalancer(String serviceName, List<InetSocketAddress> addresses);
}
