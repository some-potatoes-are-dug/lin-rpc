package com.lin.loadbalancer;

import java.net.InetSocketAddress;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/30
 **/
public interface Selector {
    /**
     * 根据服务列表执行一种算法获取一个服务节点
     * @return 具体的服务节点
     */
    InetSocketAddress getNext();

}
