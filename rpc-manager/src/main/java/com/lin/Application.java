package com.lin;

import com.lin.utils.zookeeper.ZookeeperNode;
import com.lin.utils.zookeeper.ZookeeperUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooKeeper;

import java.util.Arrays;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/9
 **/
@Slf4j
public class Application {
    public static void main(String[] args) {
        // 帮我们创建基础目录
        // rpc-metadata   (持久节点)
        //  └─ providers （持久节点）
        //  		└─ service1  （持久节点，接口的全限定名）
        //  		    ├─ node1 [data]     /ip:port
        //  		    ├─ node2 [data]
        //            └─ node3 [data]
        //  └─ consumers
        //        └─ service1
        //             ├─ node1 [data]
        //             ├─ node2 [data]
        //             └─ node3 [data]
        //  └─ config

        ZooKeeper zooKeeper = ZookeeperUtils.createZookeeper();
        //定义节点和数据
        String basePath = "/rpc-metadata";
        String providerPath = basePath + "/providers";
        String consumersPath = basePath + "/consumers";

        ZookeeperNode baseNode = new ZookeeperNode(basePath, null);
        ZookeeperNode providersNode = new ZookeeperNode(providerPath, null);
        ZookeeperNode consumersNode = new ZookeeperNode(consumersPath, null);
        Arrays.asList(baseNode, providersNode, consumersNode).forEach(node -> {
            ZookeeperUtils.createNode(zooKeeper, node, null, CreateMode.PERSISTENT);
        });
        // 关闭连接
        ZookeeperUtils.close(zooKeeper);
    }
}
