package com.lin;

import com.lin.discovery.RegistryConfig;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/9
 **/
@Slf4j
public class ConsumerApplication {
    public static void main(String[] args) {
        // 想尽一切办法获取代理对象,使用ReferenceConfig进行封装
        // reference一定用生成代理的模板方法，get()
        ReferenceConfig<HelloRpc> reference = new ReferenceConfig<>();
        reference.setInterface(HelloRpc.class);

        // 代理做了些什么，
        // 1、连接注册中心
        // 2、拉取服务列表
        // 3、选择一个服务并建立连接
        // 4、发送请求，携带一些信息（接口名，参数列表，方法的名字），获得结果
        RpcBootstrap.getInstance()
                .application("first-rpc-consumer")
                .registry(new RegistryConfig("zookeeper://192.168.88.130:2181"))
                .serialize("hessian")
                .compress("gzip")
                .group("primary")
                .reference(reference);


        System.out.println("-----------------------------------");

        HelloRpc helloRpc = reference.get();
        while(true) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < 50; i++) {
                String sayHi = helloRpc.sayHi("你好rpc");
                log.info("sayHi-->{}", sayHi);
            }
        }

    }
}
