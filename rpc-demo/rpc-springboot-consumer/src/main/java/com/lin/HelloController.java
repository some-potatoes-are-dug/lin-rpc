package com.lin;

import com.lin.annotation.RpcService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: wanglin
 * @DateTime: 2023/10/22
 **/
@RestController
public class HelloController {

    // 需要注入一个代理对象
    @RpcService
    private HelloRpc helloRpc;

    @GetMapping("hello")
    public String hello(){
        return helloRpc.sayHi("provider");
    }

}
