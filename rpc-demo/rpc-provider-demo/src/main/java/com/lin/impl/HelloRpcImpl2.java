package com.lin.impl;

import com.lin.HelloRpc2;
import com.lin.annotation.RpcApi;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/10
 **/
@RpcApi
public class HelloRpcImpl2 implements HelloRpc2 {
    @Override
    public String sayHi(String msg) {
        return "hi consumer:" + msg;
    }
}