package com.lin.impl;

import com.lin.HelloRpc;
import com.lin.annotation.RpcApi;

/**
 * @Author: wanglin
 * @DateTime: 2023/9/10
 **/
@RpcApi(group = "primary")
public class HelloRpcImpl implements HelloRpc {
    @Override
    public String sayHi(String msg) {
        return "hi consumer:" + msg;
    }
}