package com.lin.netty;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;


public class MyWatcher implements Watcher {
//            1. NodeCreated：节点被创建时触发。
//            2. NodeDeleted：节点被删除时触发。
//            3. NodeDataChanged：节点数据被修改时触发。
//            4. NodeChildrenChanged：子节点被创建或者删除时触发。
//            5. NONE： 该状态就是连接状态事件类型。前面四种是关于节点的事件，这种是连接的事件，具体由
//               Watcher.Event.KeeperState枚举维护
     @Override
     public void process(WatchedEvent event) {
         // 判断事件类型,连接类型的事件
         if(event.getType() == Event.EventType.None){
              if(event.getState() == Event.KeeperState.SyncConnected){
                  System.out.println("zookeeper连接成功");
              } else if (event.getState() == Event.KeeperState.AuthFailed){
                  System.out.println("zookeeper认证失败");
              } else if (event.getState() == Event.KeeperState.Disconnected){
                  System.out.println("zookeeper断开连接");
              }

         } else if (event.getType() == Event.EventType.NodeCreated){
             System.out.println(event.getPath() + "被创建了");
         } else if (event.getType() == Event.EventType.NodeDeleted){
             System.out.println(event.getPath() + "被删除了了");
         } else if (event.getType() == Event.EventType.NodeDataChanged){
             System.out.println(event.getPath() + "节点的数据改变了");
         } else if (event.getType() == Event.EventType.NodeChildrenChanged){
             System.out.println(event.getPath() + "子节点发生了变化");
         }



     }
}
