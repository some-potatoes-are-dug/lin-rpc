package com.lin;

import com.lin.discovery.RegistryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @Author: wanglin
 * @DateTime: 2023/10/22
 **/
@Component
@Slf4j
public class RpcStarter implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        Thread.sleep(5000);
        log.info("rpc 开始启动...");
        RpcBootstrap.getInstance()
                .application("first-rpc-provider")
                // 配置注册中心
                .registry(new RegistryConfig("zookeeper://192.168.88.130:2181"))
                .serialize("jdk")
                //扫包批量发布
                .scan("com.lin.impl")
                // 启动服务
                .start();
    }
}
